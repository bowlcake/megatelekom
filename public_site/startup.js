var express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.render('pages/lk');
});

app.get('/style', function(req, res) {
    res.sendFile(__dirname+'/views/styles/main.css');
});

app.listen(5000);
console.log('server started at 5000');