from flask import Flask
app = Flask(__name__)
 
@app.route("/", methods=["POST"])
def hello():
    import sys
    import subprocess
    process = subprocess.Popen(["git", "pull"], stdout=subprocess.PIPE)
    process = subprocess.Popen(["git", "checkout","-f", "lk_develop"], stdout=subprocess.PIPE)
    process = subprocess.Popen(["node", "startup.js"], stdout=subprocess.PIPE)
    return "Hello World!"
 
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=1111)
